/*
CRUD Operations

C - Create (Insert data)

R - Read / Retrieve (View specific document)

U - Update (Edit specific document)

D - Delete (Remove Specific )

CRUD Operations are the heart of any backend application


[SECTION] Insert documents (Create)


syntax:

db.collectionName.insertOne({object});

comparison with javascript:

object.object.method({object});

arrayName.forEach(variable);


*/

db.users.insertOne
(
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:
			{
				phone: "87654321",
				email: "janedoe@gmail.com"
			},
		courses: 
			[
				"CSS", 
				"JavaScript", 
				"Phython"
			],
		department: "none"
	}
);


/*
{
    "_id" : ObjectId("6307371b643b5bd0cfe72d46"),
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21.0,
    "contact" : 
    {
        "phone" : "87654321",
        "email" : "janedoe@gmail.com"
    },
    "courses" : 
    [ 
        "CSS", 
        "JavaScript", 
        "Phython"
    ],
    "department" : "none"
}
*/




/*
Mini Activity

            Scenario: We will create a database that will simulate a hotel database.
            1. Create a new database called "hotel".
            2. Insert a single room in the "rooms" collection with the following details:
               
                name - single
                accommodates - 2
                price - 1000
                description - A simple room with basic necessities
                rooms_available - 10
                isAvailable - false

            3. Use the "db.getCollection('users').find({})" query to check if the document is created.
           
            4. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/


//insert a single document

db.rooms.insertOne
(
	{
		name: "Jane",
		accommodates: 2,
		price: 1000,
		description: "A simple room with basic necessities",
		rooms_available: 10,
		isAvailable: false
	}
);


/*
Insert many

syntax

db.collectionName.insertMany
(
	[
		{
			objectA
		},
		{
			objectB
		}
	]
)
*/

db.users.insertMany
(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:
				{
					phone: "87654321",
					email: "stephenhawking@gmail.com"
				},
			courses: 
				[
					"Python", 
					"React", 
					"PHP"
				],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:
				{
					phone: "87654321",
					email: "neilarmstrong@gmail.com"
				},
			course: 
				[
					"React", 
					"Laravel", 
					"SASS"
				],
			department: "none"
		},
	]
);





db.rooms.insertMany
(
	[
		{
			name: "double",
			accommodates: 3,
			price: 2000,
			description: "A room fit for a small family going on a vacation",
			rooms_available: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 15,
			isAvailable: false
		},
	]
);

/*

[SECTION] Retrieve a document (Read)

syntax:

db.collectionName.find({}); // get all the documents
db.collectionName.find({field:value}); // get a specific document

*/

db.users.find({firstName:"Stephen"}); //shorthand

db.users.find({department:"none"});



/*
Find documents with multiple parameters

syntax:

db.collections.finc
(
	{
		fieldA:valueA
	},
	{
		fieldB:valueB
	}
);

*/

db.users.find
(
	{
		lastName: "Armstrong",
		age: 82
	}
);



/*
[SECTION] Updating documents (Update)

Create a document to update 



*/

db.users.insertOne
(
	{
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact:
			{
				phone: "0000000",
				email: "test@gmail.com"
			},
		courses:
			[

			],
		department: "none"
	}
);


/*

just like the find method, methods that only manipulate a single document will only


syntax:

db.collectionName.updateOne
(
	{
		criteria
	},
	{
		$set:
			{
				field:value
			}
	}
);


*/

db.users.updateOne
(
	{
		firstName: "Test"		
	},
	{
		$set:
			{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:
					{
						phone: "12345678",
						email: "bill@gmail.com",
					},
				course:
					[
						"PHP",
						"Laravel",
						"HTML"
					],
				department: "Operations",
				status: "active"
			}		
	}
);

/*
Updating multiple documents

syntax:

db.collectionName.updateMany()
(
	{
		criteria
	},
	{
		$set:
			{
				field: value
			}
	}
	
);

*/

db.users.updateMany
(
	{
		department: "none"
	},
	{
		$set:
			{
				department: "HR"
			}
	}
);


db.users.updateOne
(
	{
		_id: ObjectId
			(
				""
			)
	},
);


/*
Replace One

replace - overwrite

can be used if replacing the whole document is necessary


syntax:

db.collectionName.replaceOne
(
	{
		criteria
	},
	{
		$set:
			{
				field:value
			}
	}
);

*/


db.users.replaceOne
(
	{
		firstName: "Bill"
	}
	{

		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact:
			{
				phone: "12345678",
				email: "bill@gmail.com",
			},
		courses:
			[
				"PHP",
				"Laravel",
				"HTML"
			],
		department: "Operations",
		status: "active"
	}
);


/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/


//Updating a document

db.rooms.updateOne
(
	{
		name: "queen"		
	},
	{
		$set:
			{
				rooms_available: 0
			}		
	}
);

// Check if the document is updated
db.rooms.find
(
	{
		name: "queen"	
	}
);



/*
[SECTION] Removing documents [DELETE]


syntax:

db.collectionName.deleteOne
(
	{
		criteria
	}
);

*/

db.users.deleteOne
(
	{
		firstName: "Test"
	}
);

//find
db.rooms.find
(
	{
		firstName: "Test"	
	}
);


/*
Delete Many

be careful when using

"deleteMany"

method. If no search criteria is provided, it will delete all documents in the collection.

syntax:

db.collectionName.deletemany
(
	{
		criteria
	}
);

db.collectionName.deletemany
(
	{
	
	}
);

*/



db.users.deleteMany
(
	{
		firstName: "Test"
	}
);

//find
db.rooms.find
(
	{
		firstName: "Test"	
	}
);

/*
/*

    Mini Activity:

    1. Using the hotel database, delete all rooms with 0 available rooms.

    2. Use the find query to show all the rooms and check if the 0 available room is deleted.

    3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/



// rooms_available

db.rooms.deleteMany
(
	{
		rooms_available: 0
	}
);


// check if the 0 available room is deleted

db.rooms.find
(
	{
		rooms_available: 0
	}
);


/*
[SECTION] Advanced queries


*/


// Query an embedded document
db.users.find
(
	{
		contact:
			{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			}
	}
);

// Query on nested field
// dot notation also works when accessing a nested field.


db.users.find
(
	{
		"contact.email" : "stephenhawking@gmail.com"
	}
);


// Querying an array with Exact element

db.users.find
(
	{
		courses :
			[
				"CSS", 
        		"JavaScript", 
        		"Phython"
			]
	}
);


// this wont work

db.users.find
(
	{
		courses :
			[
				"JavaScript", 
				"CSS", 
        		"Phython"
			]
	}
);




// Querying an array disregarding the array elements order
// $all matches documents where the field contains the nested array elements

db.users.find
(
	{
		courses : 
			{
				$all:
					[
						
						"CSS", 
						"JavaScript", 
		        		"Python"
					]
			}
	}
);


// will retrieve 2 documents containing the Python has element of the nested array

db.users.find
(
	{
		courses : 
			{
				$all:
					[
						"Python"
					]
			}
	}
);


// Querying an embedded array

db.users.insertOne
(
	{
		nameArr:
			[
				{
					nameA: "Juan"
				},
				{
					nameB: "Tamad"	
				}
			]
	}
);



db.users.find
(
	{
		nameArr : 
			{
				nameA: "Juan"
			},
	}
);
